#!/bin/bash

docker buildx create --name ma-builder --driver docker-container || true
docker buildx build --builder ma-builder --no-cache --platform linux/amd64,linux/arm64 -f .devcontainer/Dockerfile --target d2 -t karreg/mkdocs-devcontainer:d2-latest --push
docker buildx build --builder ma-builder --no-cache --platform linux/amd64,linux/arm64 -f .devcontainer/Dockerfile --target drawio -t karreg/mkdocs-devcontainer:drawio-latest --push .
docker buildx build --builder ma-builder --no-cache --platform linux/amd64,linux/arm64 -f .devcontainer/Dockerfile --target allinone -t karreg/mkdocs-devcontainer:latest --push .
