# docker-mkdocs

Docker image to write mkdocs, compatible with devcontainer and gitpod. Can also be used as build image for GitLab-CI.

drawio image generation is handled so you only have to create `.drawio` files. The following samples contains the drawio extension for vscode so the file can be edited directly.

## variants

- the default variant `mkdocs-devcontainer:latest` contains drawio and d2 binaries.
- the drawio variant `mkdocs:drawio-latest` contains only drawio binaries.
- the d2 variant `mkdocs-devcontainer:d2-latest` contains only d2 binary, and is the *smallest* variant.

> ⚠️ You have to adapt your mkdocs plugins and vscode extension to the variant you are using ⚠️

## devcontainer

This devcontainer is configured to use `zsh` as main shell. You can change that part below.
THis should be compatible with `codespaces` too, maybe with a few changes in the `customizations` part.

```json
{
    "name": "mkdocs",
    "image": "karreg/mkdocs-devcontainer:latest"
    },
    "postAttachCommand": ".devcontainer/post_attach.sh",
    "customizations": {
        // Configure properties specific to VS Code.
        "vscode": {
            "settings": {
                "terminal.integrated.profiles.linux": {
                    "bash": {
                        "path": "/bin/bash"
                    },
                    "zsh": {
                        "path": "/usr/bin/zsh"
                    }
                },
                // Use bash instead if you feel like it
                "terminal.integrated.defaultProfile.linux": "zsh"
            },
            "extensions": [
                "eamodio.gitlens",
                "yzhang.markdown-all-in-one",
                "davidanson.vscode-markdownlint",
                "hediet.vscode-drawio",
                "terrastruct.d2"
            ]
        }
    },
    "userEnvProbe": "loginInteractiveShell",
    "forwardPorts": [
        8000
    ],
    "remoteUser": "gitpod",
    "containerUser": "gitpod"
}
```

## gitpod

GitPod is a bit harder to configure to set SHELL up. You can check their documentation though.

```yaml
image: karreg/mkdocs-devcontainer:latest

ports:
  - port: 8000
    onOpen: open-preview

vscode:
  extensions:
    - eamodio.gitlens
    - yzhang.markdown-all-in-one
    - davidanson.vscode-markdownlint
    - hediet.vscode-drawio,
    - terrastruct.d2
```

## gitlab-ci

```yaml
image: karreg/mkdocs-devcontainer:latest

stages:
  - deploy

variables:
  GIT_DEPTH: 0

before_script:
  - git config --global --add safe.directory '*'

pages:
  stage: deploy
  script:
  - xvfb-run -a mkdocs build
  artifacts:
    paths:
    - public
  only:
  - main
```

## recommendations

- [mkdocs-material](https://squidfunk.github.io/mkdocs-material) is a **must-have** for mkdocs. It's filled with great features
- [mkdocs-drawio-exporter](https://github.com/LukeCarrier/mkdocs-drawio-exporter) is a plugin to include and build drawio diagrams in mkdocs. Works great with the [VSCode Draw.io extension](https://marketplace.visualstudio.com/items?itemName=hediet.vscode-drawio).  
  ⚠️ Requires drawio binaries, available in the default and drawio variants of the image.
- [mkdocs-d2-plugin](https://landmaj.github.io/mkdocs-d2-plugin) is a plugin to include, embed and build d2 diagrams in mkdocs. Works great with the [VSCode D2 extension](https://marketplace.visualstudio.com/items?itemName=terrastruct.d2).  
  ⚠️ Requires d2 binary, available in the default and d2 variant of the image.
- other recommendation can be found in the [`requirements.txt`](requirements.txt) file.