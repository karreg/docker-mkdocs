# MkDocs devcontainer sample

It works!

## Drawio diagram

```markdown
![](drawio.drawio)
```

![](drawio.drawio)

!!! info
    See [mkdocs drawio exporter](https://github.com/LukeCarrier/mkdocs-drawio-exporter)

## D2 diagram (include)

```markdown
![](d2.d2){sketch="true" layout="elk"}
```

![](d2.d2){sketch="true" layout="elk"}

!!! info
    See [mkdocs d2 plugin](https://github.com/landmaj/mkdocs-d2-plugin/)

## D2 diagram (embedded)

```d2
direction: right
🧔‍♂️ -> 🌍: 👋
```

!!! info
    See [mkdocs d2 plugin](https://github.com/landmaj/mkdocs-d2-plugin/)

## D2 diagram (multiple layers)

### Root layer

```markdown
![](multi.d2)
```

![](multi.d2)

### Scenario layer

```markdown
![](multi.d2){target=wave_back_scenario}
```

![](multi.d2){target=wave_back_scenario}

### Layer layer

```markdown
![](multi.d2){target=wave_back_layer}
```

![](multi.d2){target=wave_back_layer}
